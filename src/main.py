#!/usr/bin/env python3
# Copyright (c) 2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pexpect, struct, fcntl, termios, signal

from sys import stdout
from time import sleep
from os import path, system

from ssh_authenticator import dbus
from ssh_authenticator import config

session = None

def usage(argv):
    print("%s [-c] ssh/scp arguments" % argv[0])
    print("-c: invoke scp intead of ssh")
    print("")
    system("ssh")
    print("")
    system("scp")


def get_terminal_size():
    s = struct.pack("HHHH", 0, 0, 0, 0)
    a = struct.unpack('hhhh', fcntl.ioctl(stdout.fileno(), termios.TIOCGWINSZ, s))
    return a[0], a[1]


def sigwinch_passthrough(sig, data):
    global session
    if not session.closed:
        session.setwinsize(*get_terminal_size())


def main(sysconfigdir, argv):
    global session

    if len(argv) == 1:
        usage(argv)
        return
    if argv[1] == "-c":
        argv.remove(argv[1])
        command = "scp"
    else:
        command = "ssh"
    if not stdout.isatty():
        system("/usr/bin/%s %s" % (command, " ".join(argv[1:])))
        return

    conf = config.get_config(sysconfigdir)
    code = dbus.get_code(conf["authenticator"]["ssh-key"])
    ssh_command = "/usr/bin/%s " % command
    if conf["ssh"]["ssh-accept-host"] == "yes":
        ssh_command += "-o StrictHostKeyChecking=accept-new "
    ssh_command += " ".join(argv[1:])
    session = pexpect.spawn(ssh_command)
    session.setwinsize(*get_terminal_size())
    signal.signal(signal.SIGWINCH, sigwinch_passthrough)
    while True:
        session.expect([".*%s.*" % [conf["ssh"]["ssh-prompt"]]], timeout=10)
        match = session.match.string.decode("utf-8")
        if match.find(conf["ssh"]["ssh-prompt"]) != -1:
            if code is None:
                print("Invalid authenticator code! Please check ssh-authenticator.conf")
            else:
                print(match, end="")
                for c in code:
                    print("*", end="")
                session.sendline(code)
            break
        elif match.find("Warning: Permanently added") != -1:
            pass
        else:
            print(match, end="")
            break
    session.interact()
    if command == "ssh":
        print("")

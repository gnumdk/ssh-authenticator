# Copyright (c) 2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gio, GLib

DBUS_NAME="com.belmoussaoui.Authenticator.SearchProvider"
DBUS_PATH="/com/belmoussaoui/Authenticator/SearchProvider"
DBUS_INTERFACE="org.gnome.Shell.SearchProvider2"

def get_code(id):
    bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    proxy = Gio.DBusProxy.new_sync(
        bus,
        Gio.DBusProxyFlags.NONE,
        None,
        DBUS_NAME,
        DBUS_PATH,
        DBUS_INTERFACE,
        None)

    res = proxy.GetInitialResultSet("(as)", [id])
    code = None
    if res:
        meta = proxy.GetResultMetas("(as)", res)
        if meta:
            code = meta[0]["clipboardText"]
    return code

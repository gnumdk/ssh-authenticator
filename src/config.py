# Copyright (c) 2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from configparser import ConfigParser

def get_config(sysconfigdir):
    config = ConfigParser()
    config.read("%s/ssh-authenticator.conf" % sysconfigdir)
    if not "authenticator" in config.sections():
        config["authenticator"] = {}
    if "ssh-key" not in config["authenticator"].keys():
        config["authenticator"]["ssh-key"] = "ssh"

    if "ssh" not in config.sections():
        config["ssh"] = {}
    if "ssh-prompt" not in config["ssh"].keys():
        config["ssh"]["ssh-prompt"] = "Verification code"
    if "ssh-accept-host" not in config["ssh"].keys():
        config["ssh"]["ssh-accept-host"] = "yes"
    return config

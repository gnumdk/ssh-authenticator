# ssh-authenticator


This python program allows to connect with SSH using Authenticator

You need 2FA on your SSH server.

https://apps.gnome.org/fr/app/com.belmoussaoui.Authenticator/

![GIF](https://i.imgur.com/0m7LKjm.gif)

**Usage**

Use an account named ssh in Authenticator.
```
 $ ssh-authenticator user@hostname
 $ ssh-authenticator -c /etc/fstab user@hostname:
```
You can add aliases to your ~/.bashrc
```
 alias ssh=ssh-authenticator
 alias scp="ssh-authenticator -c"
```
**Configuration**

$ gedit admin:///usr/local/etc/ssh-authenticator.conf
```
 [authenticator]
 # If you want a custom account name in Authenticator
 ssh-key=ssh

 [ssh]
 # If you need a different 2FA prompt
 ssh-prompt=Verification code
 # If you want us to auto accept host key (on first connection)
 ssh-accept-host=yes
```

**Build**

```
 git clone https://gitlab.gnome.org/gnumdk/ssh-authenticator.git
 cd ssh-authenticator
 meson build
 sudo ninja -C build install
```
